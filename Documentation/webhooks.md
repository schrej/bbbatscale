# Webhooks

Webhooks may be used for integrating with other apps and services.  BBBatScale
can send a HTTP request to a configured endpoint whenever one of a selected
number of events is signaled.

## Prerequisites

Webhooks are only queued for later execution from within the Django app. The
actual processing takes place in separate worker processes. These can be
invoked through `./manage.py rqworker --with-scheduler`. Arrange for at least one
such worker to be running for webhooks to be processed.

### Configuration

Set the `WEBHOOKS` environment variable to `enabled` to activate webhook
support. `REDIS_HOST` and `REDIS_PORT` should point to some Redis server (also
set `REDIS_PASSWORD` if applicable). The Redis database used for queueing
webhook execution (and possibly scheduling retries) can be changed from the
default value using the `WEBHOOKS_REDIS_DB` variable.

## Request format

Outgoing webhooks are sent with content type `application/json`. The request body
contains a JSON dictionary with the following structure:

```json
{
  "event": "SUPPORT_CHAT_INCOMING_MESSAGE",
  "ts": 1603593349,
  "payload": <event-specific payload dictionary>
}
```

The `event` key signifies the event that lead to this webhook's execution. `Ts`
is a timestamp in seconds since the Unix epoch. Each event may document
additional payload data sent in a dictionary under the `payload` key.  The
exact format depends on the event.

## Authenticating webhooks

To ensure that received requests were in fact sent by BBBatScale you can
configure a shared secret when initially setting up the webhook.  This secret
is used as a key to compute a Keyed-Hash Message Authentication Code
(HMAC-SHA512) over the request body with an additional timestamp included.

Hence, the message fed into the HMAC is constructed as follows:

`timestamp '.' request-body`

`Timestamp` is an ASCII decimal string representing the number of seconds since
the Unix epoch whereas `request-body` is the body bytes from the request.

The resulting MAC tag is hex-encoded and sent as part of the `X-Hook-Signature`
HTTP request header.

`X-Hook-Signature: t=1603593349,v1=<mac-tag>`

Future versions may introduce newer message authentication schemes but as of
this writing, there's only a `v1`.

To verify the MAC, split the header value on ',' and pull out the
`t=<timestamp>` and `v1=<mac-tag>` key/value pairs. Ignore other K/V pairs
potentially present in the header. Recompute the MAC as described above and
compare to the expected tag taken from the header. If it matches, decide
whether you want to accept hooks from the specified point in time. The
acceptable time delta depends on the level of trust you have in your clock
synchronization mechanism.

Note that this scheme is very close to the one used by
[Stripe](https://stripe.com/docs/webhooks/signatures) for their webhook
implementation.
