import pytest
from django.conf import settings
from django.contrib.auth.models import Group

from core.models import Tenant, Room, User, get_default_room_config, Meeting
from core.services import get_join_password


@pytest.fixture(scope='function')
def moderator_group(db) -> Group:
    return Group.objects.create(name=settings.MODERATORS_GROUP)


@pytest.fixture(scope='function')
def example(db) -> Tenant:
    return Tenant.objects.create(
        name="example",
    )


@pytest.fixture(scope='function')
def user_bbb_mod(db, moderator_group) -> User:
    user = User.objects.create_user(
        username="bbb_mod",
        email="bbb_mod@example.org",
        password="bbb_mod"
    )
    user.groups.add(moderator_group)
    return user


@pytest.fixture(scope='function')
def user_no_bbb_mod(db) -> User:
    user = User.objects.create_user(
        username="no_bbb_mod",
        email="no_bbb_mod@example.org",
        password="no_bbb_mod"
    )
    return user


@pytest.fixture(scope='function')
def user_superuser(db) -> User:
    user = User.objects.create(
        username="user_superuser",
        email="user_superuser@example.org",
        is_superuser=True
    )
    return user


@pytest.fixture(scope='function')
def user_staff(db) -> User:
    user = User.objects.create(
        username="user_staff",
        email="user_staff@example.org",
        is_superuser=False,
        is_staff=True
    )
    return user


@pytest.fixture(scope='function')
def room(db, example) -> Room:
    return Room.objects.create(
        tenant=example,
        name="room_every_one_can_start",
        attendee_pw="test_attendee_password",
        moderator_pw="test_moderator_password",
        all_moderator=False,
        everyone_can_start=True,
        config=get_default_room_config()
    )


@pytest.fixture(scope='function')
def room_all_are_mods(db, example) -> Room:
    return Room.objects.create(
        tenant=example,
        name="room_all_mods",
        attendee_pw="test_attendee_password",
        moderator_pw="test_moderator_password",
        all_moderator=True,
        everyone_can_start=True,
        config=get_default_room_config()
    )


@pytest.fixture(scope='function')
def room_ask_mod(db, example) -> Room:
    return Room.objects.create(
        tenant=example,
        name="room_ask_mod",
        attendee_pw="test_attendee_password",
        moderator_pw="test_moderator_password",
        all_moderator=True,
        everyone_can_start=True,
        guest_policy="ASK_MODERATOR",
        config=get_default_room_config()
    )


@pytest.mark.django_db
def test_get_join_password(user_bbb_mod, user_no_bbb_mod, user_superuser, user_staff, room, room_all_are_mods,
                           room_ask_mod):
    Meeting.objects.create(room_name=room.name, creator=user_bbb_mod.username)

    assert get_join_password(user_bbb_mod, room, user_bbb_mod.__str__()) == "test_moderator_password"
    assert get_join_password(user_no_bbb_mod, room, user_no_bbb_mod.__str__()) == "test_attendee_password"
    assert "test_moderator_password" == get_join_password(user_superuser, room, user_superuser.__str__())
    assert get_join_password(user_staff, room, user_staff.__str__()) == "test_moderator_password"

    Meeting.objects.create(room_name=room_all_are_mods.name, creator=user_bbb_mod.username)

    assert get_join_password(user_bbb_mod, room_all_are_mods, user_bbb_mod.__str__()) == "test_moderator_password"
    assert get_join_password(user_no_bbb_mod, room_all_are_mods, user_no_bbb_mod.__str__()) == "test_moderator_password"
    assert get_join_password(user_superuser, room_all_are_mods, user_superuser.__str__()) == "test_moderator_password"
    assert get_join_password(user_staff, room_all_are_mods, user_staff.__str__()) == "test_moderator_password"

    Meeting.objects.create(room_name=room_ask_mod.name, creator=user_bbb_mod.username)

    assert get_join_password(user_bbb_mod, room_ask_mod, user_bbb_mod.username) == "test_moderator_password"
    assert get_join_password(user_no_bbb_mod, room_ask_mod, user_no_bbb_mod.username) == "test_attendee_password"
    assert get_join_password(user_superuser, room_ask_mod, user_superuser.username) == "test_attendee_password"
    assert get_join_password(user_staff, room_ask_mod, user_staff.username) == "test_attendee_password"
