import pytest
from django.conf import settings
from core.services import create_meeting_and_bbb_parameter
from core.models import MoodleRoom, Room, Tenant, get_default_room_config


@pytest.fixture(scope='function')
def example(db) -> Tenant:
    return Tenant.objects.create(
        name="example",
    )


@pytest.fixture(scope='function')
def create_parameter_moodle_room(db):
    return {
        "name": "sample_create_parameter_name",
        "meeting_id": "123-456-789",
        "attendee_pw": "test_attendee_pw",
        "moderator_pw": "test_moderator_pw",
        "mute_on_start": "True",
        "record": "true",
        "welcome_message": None,
        "dialNumber": None,
    }


@pytest.fixture(scope='function')
def wrong_create_parameters(db):
    return {
        "_name_": "wrong_param_name",
        "_meetingID_": "123-456-789",
        "_attendeePW_": "test_attendee_pw",
        "_moderator_pw_": "test_moderator_pw",
        "_mute_on_start": "True",
    }


@pytest.fixture(scope='function')
def create_parameter_not_moodle_room(db):
    return {
        "name": "sample_create_parameter_name",
        "meeting_id": "123-456-789",
        "attendee_pw": "test_attendee_pw",
        "moderator_pw": "test_moderator_pw",
        "logoutUrl": settings.BASE_URL,
        "mute_on_start": True,
        "access_code": "123",
        "access_code_guests": "123",
        "guest_policy": "ASK_MODERATOR",
        "disable_cam": True,
        "disable_mic": True,
        "disable_note": True,
        "disable_public_chat": True,
        "disable_private_chat": True,
        "allow_recording": True,
        "creator": "sample_meeting_creator",
        "all_moderator": True,
        "allow_guest_entry": True,
        "url": "http://bbb-test-create-params",
        "welcome_message": None,
        "dialNumber": None,
        "maxParticipants": None,
    }


@pytest.fixture(scope='function')
def moodle_room(db, example) -> MoodleRoom:
    return MoodleRoom.objects.create(
        tenant=example,
        meeting_id="99999999",
        name="D14/02.14",
        attendee_pw="test_attendee_password_moodle",
        moderator_pw="test_moderator_password_moodle",
        all_moderator=True,
        config=get_default_room_config(),
    )


@pytest.fixture(scope='function')
def no_moodle_room(db, example) -> Room:
    return Room.objects.create(
        tenant=example,
        meeting_id="1234567891011",
        name="not_moodle_room",
        attendee_pw="test_attendee_password",
        moderator_pw="test_moderator_password",
        all_moderator=False,
        everyone_can_start=True,
        config=get_default_room_config()
    )


def test_create_meeting_and_bbb_parameter_moodle_room(create_parameter_moodle_room, wrong_create_parameters,
                                                      create_parameter_not_moodle_room, moodle_room, no_moodle_room):
    # if room is moodle room
    assert create_meeting_and_bbb_parameter(moodle_room, create_parameter_moodle_room)[
               'name'] == "sample_create_parameter_name"
    assert create_meeting_and_bbb_parameter(moodle_room, create_parameter_moodle_room)['meeting_id'] == "123-456-789"
    assert create_meeting_and_bbb_parameter(moodle_room, create_parameter_moodle_room)['meta_creator'] == "Moodle"
    assert create_meeting_and_bbb_parameter(moodle_room, create_parameter_moodle_room)['meta_allowrecording']
    assert create_meeting_and_bbb_parameter(moodle_room, create_parameter_moodle_room)[
               'attendee_pw'] == "test_attendee_pw"
    assert create_meeting_and_bbb_parameter(moodle_room, create_parameter_moodle_room)[
               'moderator_pw'] == "test_moderator_pw"
    assert create_meeting_and_bbb_parameter(moodle_room, create_parameter_moodle_room)['mute_on_start']
    assert create_meeting_and_bbb_parameter(moodle_room, create_parameter_moodle_room)['record']
    assert create_meeting_and_bbb_parameter(moodle_room, create_parameter_moodle_room)['welcome_message'] is None
    assert create_meeting_and_bbb_parameter(moodle_room, create_parameter_moodle_room)['dialNumber'] is None

    # if passed parameters are wrong
    assert create_meeting_and_bbb_parameter(moodle_room, wrong_create_parameters) is None

    # if room is not moodle room
    assert create_meeting_and_bbb_parameter(no_moodle_room, create_parameter_not_moodle_room)[
               'name'] == "sample_create_parameter_name"
    assert create_meeting_and_bbb_parameter(no_moodle_room, create_parameter_not_moodle_room)[
               'meetingID'] == "123-456-789"
    assert create_meeting_and_bbb_parameter(no_moodle_room, create_parameter_not_moodle_room)[
               'attendeePW'] == "test_attendee_pw"
    assert create_meeting_and_bbb_parameter(no_moodle_room, create_parameter_not_moodle_room)[
               'moderatorPW'] == "test_moderator_pw"
    assert create_meeting_and_bbb_parameter(no_moodle_room, create_parameter_not_moodle_room)['muteOnStart'] == "true"
    assert create_meeting_and_bbb_parameter(no_moodle_room, create_parameter_not_moodle_room)[
               'lockSettingsDisableCam'] == "true"
    assert create_meeting_and_bbb_parameter(no_moodle_room, create_parameter_not_moodle_room)[
               'lockSettingsDisableMic'] == "true"
    assert create_meeting_and_bbb_parameter(no_moodle_room, create_parameter_not_moodle_room)[
               'lockSettingsDisableNote'] == "true"
    assert create_meeting_and_bbb_parameter(no_moodle_room, create_parameter_not_moodle_room)[
               'lockSettingsDisablePublicChat'] == "true"
    assert create_meeting_and_bbb_parameter(no_moodle_room, create_parameter_not_moodle_room)[
               'lockSettingsDisablePrivateChat'] == "true"
    assert create_meeting_and_bbb_parameter(no_moodle_room, create_parameter_not_moodle_room)[
               'guestPolicy'] == "ASK_MODERATOR"
    assert create_meeting_and_bbb_parameter(no_moodle_room, create_parameter_not_moodle_room)['record'] == "true"
    assert create_meeting_and_bbb_parameter(no_moodle_room, create_parameter_not_moodle_room)[
               'allowStartStopRecording'] == "true"
    assert create_meeting_and_bbb_parameter(no_moodle_room, create_parameter_not_moodle_room)[
               'meta_creator'] == "sample_meeting_creator"
    assert create_meeting_and_bbb_parameter(no_moodle_room, create_parameter_not_moodle_room)['meta_muteonstart']
    assert create_meeting_and_bbb_parameter(no_moodle_room, create_parameter_not_moodle_room)['meta_allmoderator']
    assert create_meeting_and_bbb_parameter(no_moodle_room, create_parameter_not_moodle_room)[
               'meta_guestpolicy'] == "ASK_MODERATOR"
    assert create_meeting_and_bbb_parameter(no_moodle_room, create_parameter_not_moodle_room)['meta_allowguestentry']
    assert create_meeting_and_bbb_parameter(no_moodle_room, create_parameter_not_moodle_room)[
               'meta_accesscode'] == "123"
    assert create_meeting_and_bbb_parameter(no_moodle_room, create_parameter_not_moodle_room)[
               'meta_accesscodeguests'] == "123"
    assert create_meeting_and_bbb_parameter(no_moodle_room, create_parameter_not_moodle_room)['meta_disablecam']
    assert create_meeting_and_bbb_parameter(no_moodle_room, create_parameter_not_moodle_room)['meta_disablemic']
    assert create_meeting_and_bbb_parameter(no_moodle_room, create_parameter_not_moodle_room)['meta_disablenote']
    assert create_meeting_and_bbb_parameter(no_moodle_room, create_parameter_not_moodle_room)['meta_disableprivatechat']
    assert create_meeting_and_bbb_parameter(no_moodle_room, create_parameter_not_moodle_room)['meta_disablepublicchat']
    assert create_meeting_and_bbb_parameter(no_moodle_room, create_parameter_not_moodle_room)['meta_allowrecording']
    assert create_meeting_and_bbb_parameter(no_moodle_room, create_parameter_not_moodle_room)[
               'meta_url'] == "http://bbb-test-create-params"
