import pytest
from core.services import translate_bbb_meta_data


@pytest.fixture(scope='function')
def metadata(db):
    return {
        'muteonstart': False,
        'allmoderator': "True",
        'guestpolicy': "ASK_MODERATOR",
        'allowguestentry': "True",
        'accesscode': 123456789,
        'accesscodeguests': 123456789,
        'disablecam': "True",
        'disablemic': "True",
        'disablenote': "True",
        'disableprivatechat': "True",
        'disablepublicchat': "True",
        'allowrecording': "True",
        'url': "http://test-create-meeting-url",
        "maxparticipants": None,
    }


@pytest.fixture(scope='function')
def metadata_key_error(db):
    return {
        'muteonstart': "True",
        'allmoderator': "True",
        'guestpolicy': "ASK_MODERATOR",
        'allowguestentry': "True",
        'accesscode': 123456789,
        'accesscodeguests': 123456789,
        'disablecam': "True",
        'wrong_key': "True",
        'disablenote': "True",
        'disableprivatechat': "True",
        'disablepublicchat': "True",
        'allowrecording': "True",
        'url': "http://test-create-meeting-url",
        "maxParticipants": None,
    }


def test_translate_bbb_meta_data(metadata, metadata_key_error):
    # key error in metadata
    assert translate_bbb_meta_data(metadata_key_error) == {}

    # right meta data
    assert translate_bbb_meta_data(metadata)['mute_on_start'] is False
    assert translate_bbb_meta_data(metadata)['all_moderator']
    assert translate_bbb_meta_data(metadata)['guest_policy'] == "ASK_MODERATOR"
    assert translate_bbb_meta_data(metadata)['allow_guest_entry']
    assert translate_bbb_meta_data(metadata)['access_code'] == 123456789
    assert translate_bbb_meta_data(metadata)['access_code_guests'] == 123456789
    assert translate_bbb_meta_data(metadata)['disable_cam']
    assert translate_bbb_meta_data(metadata)['disable_mic']
    assert translate_bbb_meta_data(metadata)['disable_note']
    assert translate_bbb_meta_data(metadata)['disable_private_chat']
    assert translate_bbb_meta_data(metadata)['disable_public_chat']
    assert translate_bbb_meta_data(metadata)['allow_recording']
    assert translate_bbb_meta_data(metadata)['url'] == "http://test-create-meeting-url"
    assert translate_bbb_meta_data(metadata)['maxParticipants'] is None
